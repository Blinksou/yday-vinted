<?php

declare(strict_types=1);

namespace App\Security\Listener;

use App\Mailer\User\UserMailer;
use App\Security\Event\UserCreatedEvent;
use App\Security\Service\TokenService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener]
final class OnUserCreatedListener
{
    public function __construct(
        private readonly TokenService           $tokenService,
        private readonly EntityManagerInterface $manager,
        private readonly UserMailer             $userMailer
    )
    {
    }

    public function __invoke(UserCreatedEvent $event): void
    {
        $user = $event->getUser();

        $user->setValidationToken($this->tokenService->generateToken());

        $this->manager->flush();
        
        $this->userMailer->sendValidationEmail($user);
    }
}
