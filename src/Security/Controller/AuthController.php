<?php

declare(strict_types=1);

namespace App\Security\Controller;

use App\Security\Form\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

final class AuthController extends AbstractController
{
    public function __construct(
        private readonly FormFactoryInterface $formFactory,
        private readonly TranslatorInterface $translator,
    )
    {
    }

    #[Route(path: '/login', name: 'auth_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_home');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $loginForm = $this->formFactory->createNamed('', LoginType::class, [
            'email' => $lastUsername
        ]);

        if ($error) {
            $loginForm->addError(new FormError(
                $this->translator->trans(
                    $error->getMessageKey(), $error->getMessageData(), 'security'
                ),
                $error->getMessageKey(),
                $error->getMessageData()
            ));
        }

        return $this->renderForm('security/login.html.twig', compact('loginForm'));
    }


    #[Route(path: '/logout', name: 'auth_logout')]
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
