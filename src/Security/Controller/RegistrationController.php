<?php

declare(strict_types=1);

namespace App\Security\Controller;

use App\Entity\User;
use App\Security\Event\UserCreatedEvent;
use App\Security\Form\RegistrationFormType;
use App\Security\Form\RegistrationType;
use App\Security\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

final class RegistrationController extends AbstractController
{
    public function __construct(
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly EntityManagerInterface   $entityManager,
        private readonly UserService              $userService,
    )
    {
    }

    #[Route('/register', name: 'auth_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        $user = new User();
        $registrationForm = $this->createForm(RegistrationType::class, $user);
        $registrationForm->handleRequest($request);

        if ($registrationForm->isSubmitted() && $registrationForm->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $registrationForm->get('plainPassword')->getData()
                )
            );

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->eventDispatcher->dispatch(new UserCreatedEvent($user));

            return $this->redirectToRoute('auth_login');
        }

        return $this->renderForm('security/register.html.twig', compact('registrationForm'));
    }

    #[Route('/register/validate/{token}', name: 'auth_register_validate')]
    #[ParamConverter('user', options: ['mapping' => ['token' => 'validationToken']])]
    public function registerValidateAction(User $user): Response
    {
        $this->userService->validateRegistration($user);

        $this->addFlash('success', 'Your account has been validated!');

        return $this->redirectToRoute('auth_login');
    }
}
