<?php

declare(strict_types=1);

namespace App\Security\Service;

use App\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;

final class UserService
{
    public function __construct(
        private EntityManagerInterface $entityManager
    )
    {
    }

    public function validateRegistration(User $user): void
    {
        $user->setValidatedAt(new DateTimeImmutable());
        $user->setValidationToken(null);

        $this->entityManager->flush();
    }
}
