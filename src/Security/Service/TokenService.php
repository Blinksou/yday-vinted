<?php

declare(strict_types=1);

namespace App\Security\Service;

final class TokenService
{
    public function generateToken(): string
    {
        return bin2hex(random_bytes(16));
    }
}
