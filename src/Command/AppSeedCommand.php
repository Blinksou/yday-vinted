<?php

declare(strict_types=1);

namespace App\Command;

use App\Factory\ItemFactory;
use App\Factory\TagFactory;
use App\Factory\UserFactory;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:seed',
    description: 'Seed the database',
)]
final class AppSeedCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Seeding the database');

        UserFactory::createMany(10);

        TagFactory::createMany(50);

        ItemFactory::createMany(100);

        $io->success('Done!');

        return Command::SUCCESS;
    }
}
