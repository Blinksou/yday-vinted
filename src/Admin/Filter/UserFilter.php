<?php

declare(strict_types=1);

namespace App\Admin\Filter;

use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Filter\FilterInterface;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FieldDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\FilterDataDto;
use EasyCorp\Bundle\EasyAdminBundle\Filter\FilterTrait;
use EasyCorp\Bundle\EasyAdminBundle\Form\Filter\Type\TextFilterType;

final class UserFilter implements FilterInterface
{
    use FilterTrait;

    private string $userRelation;

    public static function new(string $propertyName, string $userRelation, ?string $label = null): self
    {
        return (new self())
            ->setFilterFqcn(self::class)
            ->setUserRelation($userRelation)
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setFormType(TextFilterType::class);
    }

    private function setUserRelation(string $userRelation): self
    {
        $this->userRelation = $userRelation;

        return $this;
    }

    public function apply(QueryBuilder $queryBuilder, FilterDataDto $filterDataDto, ?FieldDto $fieldDto, EntityDto $entityDto): void
    {
        $doJoin = true;

        $joinParts = $queryBuilder->getDQLPart('join')['entity'] ?? null;

        if ($joinParts) {
            foreach ($joinParts as $joinPart) {
                if ($joinPart->getAlias() === $this->userRelation) {
                    $doJoin = false;
                }
            }
        }

        if ($doJoin) {
            $queryBuilder
                ->innerJoin(
                    sprintf('entity.%s', $this->userRelation), $this->userRelation);
        }

        $queryBuilder
            ->andWhere(
                sprintf(
                    '%s.%s %s :%s',
                    $this->userRelation,
                    $filterDataDto->getProperty(),
                    $filterDataDto->getComparison(),
                    $filterDataDto->getParameterName()
                )
            )
            ->setParameter($filterDataDto->getParameterName(), $filterDataDto->getValue());
    }
}
