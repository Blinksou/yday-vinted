<?php

declare(strict_types=1);

namespace App\Admin\Controller;

use App\Admin\Form\AppSettingsType;
use App\Entity\AppSettings;
use App\Repository\AppSettingsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/custom/settings', name: 'admin_custom_settings')]
final class CustomSettingsAction extends AbstractController
{
    public function __construct(
        private readonly AppSettingsRepository  $appSettingsRepository,
        private readonly EntityManagerInterface $entityManager,
    )
    {
    }

    public function __invoke(Request $request): Response
    {
        $appSettings = $this->appSettingsRepository->findOneBy([]);
        if (!$appSettings) {
            $appSettings = new AppSettings();
            $this->entityManager->persist($appSettings);
        }

        $form = $this->createForm(AppSettingsType::class, $appSettings);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();
        }

        return $this->renderForm('admin/custom_settings.html.twig', compact('form'));
    }
}
