<?php

declare(strict_types=1);

namespace App\Admin\Controller\Item;

use App\Admin\Filter\UserFilter;
use App\Entity\Item;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

final class ItemCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Item::class;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('name')
            ->add('description')
            ->add('seller')
            ->add(UserFilter::new('firstName', 'seller', 'First Name'))
            ->add(UserFilter::new('lastName', 'seller', 'Last Name'));
    }

    public function configureFields(string $pageName): iterable
    {
        yield from $this->getItemDetailsFields();
        yield from $this->getAssociatedFields();
    }

    private function getItemDetailsFields(): iterable
    {
        yield FormField::addTab('Item details');

        yield TextField::new('name');
        yield MoneyField::new('price')
            ->setCurrency('EUR');

        yield TextareaField::new('description')
            ->onlyOnForms();
    }

    private function getAssociatedFields(): iterable
    {
        yield FormField::addTab('Associated');

        yield AssociationField::new('seller');
        yield AssociationField::new('tags');
    }
}
