<?php

namespace App\Factory;

use App\Entity\Item;
use App\Repository\ItemRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Item>
 *
 * @method static Item|Proxy createOne(array $attributes = [])
 * @method static Item[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Item[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Item|Proxy find(object|array|mixed $criteria)
 * @method static Item|Proxy findOrCreate(array $attributes)
 * @method static Item|Proxy first(string $sortedField = 'id')
 * @method static Item|Proxy last(string $sortedField = 'id')
 * @method static Item|Proxy random(array $attributes = [])
 * @method static Item|Proxy randomOrCreate(array $attributes = [])
 * @method static Item[]|Proxy[] all()
 * @method static Item[]|Proxy[] findBy(array $attributes)
 * @method static Item[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Item[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemRepository|RepositoryProxy repository()
 * @method Item|Proxy create(array|callable $attributes = [])
 */
final class ItemFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->text(),
            'price' => self::faker()->randomFloat(),
            'tags' => TagFactory::randomRange(1, 8),
            'seller' => UserFactory::random(),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this// ->afterInstantiate(function(Item $item): void {})
            ;
    }

    protected static function getClass(): string
    {
        return Item::class;
    }
}
