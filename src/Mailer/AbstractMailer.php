<?php

declare(strict_types=1);

namespace App\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface as SymfonyMailerInterface;
use Symfony\Component\Mime\Address;

abstract class AbstractMailer implements MailerInterface
{
    public const FROM = 'no-reply@yday.com';
    public const FROM_NAME = 'Yday';
    public const SUBJECT = '[Yday]';

    public function __construct(
        private readonly SymfonyMailerInterface $mailer,
        private ?TemplatedEmail                 $email = null,
        private string                          $to = '',
        private string                          $subject = '',
        private string                          $template = '',
        private array                           $context = [],
    )
    {
    }

    public function build(): self
    {
        $this->email = (new TemplatedEmail())
            ->from(new Address(self::FROM, self::FROM_NAME))
            ->to($this->getTo())
            ->subject(sprintf('%s | %s', self::SUBJECT, $this->subject))
            ->htmlTemplate($this->getTemplate())
            ->context($this->getContext());


        return $this;
    }

    public function send(): void
    {
        if (!$this->email) {
            return;
        }

        $this->mailer->send($this->email);
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function setContext(array $context): self
    {
        $this->context = $context;

        return $this;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function setTo(string $to): self
    {
        $this->to = $to;

        return $this;
    }
}
