<?php

declare(strict_types=1);

namespace App\Mailer\User;

use App\Entity\User;
use App\Mailer\AbstractMailer;

final class UserMailer extends AbstractMailer
{
    public function sendValidationEmail(User $user): void
    {
        $this
            ->setTo($user->getEmail())
            ->setSubject('Validate your account')
            ->setTemplate('emails/security/validate_account.html.twig')
            ->setContext(compact('user'))
            ->build()
            ->send();
    }

    public function sendEmail(): void
    {
        // TODO: Implement sendEmail() method.


    }
}
