<?php

declare(strict_types=1);

namespace App\Mailer;

interface MailerInterface
{
    public function build(): self;

    public function send(): void;
}
