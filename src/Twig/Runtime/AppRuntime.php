<?php

namespace App\Twig\Runtime;

use App\Entity\AppSettings;
use App\Repository\AppSettingsRepository;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    public function __construct(
        private readonly AppSettingsRepository $appSettingsRepository
    )
    {
    }

    public function divideByTwo(int $number): int
    {
        return $number / 2;
    }

    public function getAppSettings(): ?AppSettings
    {
        return $this->appSettingsRepository->findOneBy([]);
    }
}
