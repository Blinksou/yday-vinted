<?php

namespace App\Twig\Extension;

use App\Twig\Runtime\AppRuntime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('divide_by_2', [AppRuntime::class, 'divideByTwo']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('app_get_settings', [AppRuntime::class, 'getAppSettings']),
        ];
    }
}
