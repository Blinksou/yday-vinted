DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# 'ssh_copy': otherwise .ssh will not have the correct permissions. so we need to store them elsewhere and copy them.
docker run --rm -it \
    -v ~/.ssh:/root/ssh_copy \
    -v "${DIR}":/usr/src/app vinted-capistrano \
     bash -c "cp -rf /root/ssh_copy /root/.ssh && cap -f Capfile $*"
