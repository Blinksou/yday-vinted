# Default value for keep_releases is 5
set :keep_releases, 3

# Default branch is :master
set :branch, "main"
