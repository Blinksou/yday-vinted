set :application, "vinted"
set :deploy_to, "/var/www/vinted"
set :repo_url, "git@gitlab.com:Blinksou/yday-vinted.git"

# Default branch is :master
set :branch, "main"

set :symfony_env, "prod"

append :linked_dirs, 'var/log', 'var/sessions', 'var/cache', 'public/uploads'
append :linked_files, '.env'

set :composer_install_flags, ' --no-dev --prefer-dist --no-interaction --optimize-autoloader --no-cache'

server 'blinks.dev',
  port: 22,
  user: 'ju',
  roles: %w{ju}

# CUSTOM TASKS
namespace :vinted do

  desc 'Install assets'
  task :build_front do
    on roles(:ju) do
      within release_path do
       execute :yarn, "install"
       execute :yarn, "build"
       execute :rm, "-rf node_modules"
       symfony_console('cache:clear')
      end
    end
  end

  desc 'Database update'
  task :db_update do
    on roles(:ju) do
#      symfony_console('doctrine:migrations:migrate', '-n')
      symfony_console('doctrine:schema:update', '--force')
    end
  end
end

# Update schema
after 'deploy:updated', 'vinted:db_update'

# Generate assets
after 'deploy:updated', 'vinted:build_front'
